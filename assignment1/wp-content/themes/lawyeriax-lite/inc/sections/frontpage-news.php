<?php
/**
 * Frontpage News Section
 *
 * @package LawyeriaX
 */

if ( ! function_exists( 'lawyeriax_news_section' ) ) :
	/**
	 * Latest news section
	 */
	function lawyeriax_news_section() {
		  global $wp_customize;
		  $rnews_toggle  = get_theme_mod( 'lawyeriax_news_toggle', 0 );
		  $news_heading = get_theme_mod( 'news_heading', esc_html__( 'Latest News','lawyeriax-lite' ) );
		  $news_category = get_theme_mod( 'news_category' );
		  $args = array(
		'cat'       => $news_category,
		'showposts' => 3,
		  );

		if ( ! $rnews_toggle && isset( $rnews_toggle ) ) : ?>

				  <section id="news" class="home-section news">

				<?php 	elseif ( isset( $wp_customize ) ) : ?>

	<section id="news" class="home-section news hidden-in-customizer">

	<?php endif; ?>

			<?php if ( ( ! $rnews_toggle) || isset( $wp_customize ) ) : ?>

			<div class="container">
		<?php
		if ( ! empty( $news_heading ) ) { ?>
				 <div class="home-section-title-wrap">
		              <h2 class="home-section-title"> <?php echo wp_kses_post( force_balance_tags( $news_heading ) ); ?></h2>
			 </div>
			<?php	} ?>

				<div class="home-section-inner latest-news">
					<!-- Posts Loop -->

			<?php
			// the query
			$the_query = new WP_Query( $args ); ?>

			<?php if ( $the_query->have_posts() ) : ?>

		  	<!-- pagination here -->

		  	<!-- the loop -->
			<?php while ( $the_query->have_posts() ) : $the_query->the_post();
				get_template_part( 'template-parts/content-home-single', get_post_format() );
		  endwhile; ?>
		  	<!-- end of the loop -->

		  	<?php wp_reset_postdata(); ?>

			<?php endif; ?>

				</div>

				<div class="col-sm-10 col-sm-offset-1 section-line"></div>
				</div><!-- .container -->
		</section>

	<?php endif;
	}
endif;
