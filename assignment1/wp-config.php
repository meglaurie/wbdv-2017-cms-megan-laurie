<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'assignment1');

/** MySQL database username */
define('DB_USER', 'dbAssign1');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'UG7+/dZ#wBNLLgwB<Wz4SR-=.7hj}x=wQr0j+ki uTHx]?+E+$H7aPovMT-.uB<o');
define('SECURE_AUTH_KEY',  'EV V^@8k!j#o.PE4O_{t5A~/yB`L<|7bEsaZI@2`0]i/~{EK]LG4WVZMI:o|ZuJ|');
define('LOGGED_IN_KEY',    '$n&YFpN%3v(+ceD }V&N ^St->FXc&-[v9.~JlNM@21S?;+1 P3IJx0HkTVDv45&');
define('NONCE_KEY',        '0|<HQ4G|kwFN9i}m!wWq4FS<lQ1AR7&zWWlO]@ai3vOA3p,WyUphS[oD8@_miiSc');
define('AUTH_SALT',        '2da4rnZ- MR(l^|y+rGHYV=Lch}h1svPz5)z)#`~PX=A4leH+e:V41p -1Ue6Jh2');
define('SECURE_AUTH_SALT', 'B-7sOP|fp?Pno#J.wziS[8wlD#QZy{V*@Jf|*GYeDkl#HzJA!I1b@%Tif{|g4ij`');
define('LOGGED_IN_SALT',   '+]qxNH~`lqf.WMPw+BL49d0B:|t)-bV2VGrVxj-:eJ3/4ko|DA/iGyn.3q4R]Xub');
define('NONCE_SALT',       '`oj/}c438:b1A+[[_][{AE.Y|l>A2HnE:^=}_D*V5LjQZii(^=e jp%4K$sr|[fG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
