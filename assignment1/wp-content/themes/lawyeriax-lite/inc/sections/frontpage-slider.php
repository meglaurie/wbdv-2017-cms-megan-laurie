<?php
/**
 * Frontpage Slider Section
 *
 * @package LawyeriaX
 */

if ( ! function_exists( 'lawyeriax_slider_section' ) ) :
	/**
	 * Slider section
	 */
	function lawyeriax_slider_section() {

		$default                    = lawyeriax_get_slider_shortcode();
		$lawyeriax_slider_shortcode = get_theme_mod( 'lawyeriax_slider_shortcode', $default );

		if ( ! empty( $lawyeriax_slider_shortcode ) ) {
			echo do_shortcode( $lawyeriax_slider_shortcode );

			return;
		}

		global $wp_customize;
		$slider_toggle = get_theme_mod( 'lawyeriax_slider_toggle', 0 );

		$slider_content = get_theme_mod( 'lawyeriax_slider_content', apply_filters( 'lawyeriax_filter_slider_defaults', '' ) );

		$slides_counter = 0;
		$var1           = 0;
		?>

		<?php if ( ! $slider_toggle && isset( $slider_toggle ) ) { ?>

			<section id="slider" class="header-slider">

		<?php } elseif ( isset( $wp_customize ) ) { ?>

			<section id="slider" class="header-slider hidden-in-customizer">

		<?php } ?>

		<?php if ( ( ( ! $slider_toggle ) || isset( $wp_customize ) ) && ( ! empty( $slider_content ) ) ) : ?>

		<div id="main-slider" class="carousel slide" data-ride="carousel">

			<?php

			$slider_content_decoded = json_decode( $slider_content );

			if ( ! empty( $slider_content_decoded ) ) { ?>

			<div class="carousel-inner" role="listbox">

				<?php foreach ( $slider_content_decoded as $slider_content ) {
					if ( $var1 == 0 ) { ?>
						<div class="item active" style="background-image: url('<?php
						echo esc_url(
						( function_exists( 'pll__' ) ?
						pll__( $slider_content->image_url ) :
						apply_filters( 'wpml_translate_single_string', $slider_content->image_url, 'Repeater', 'Slider Content Image - ' . $slider_content->id )
						) ); ?>' );">
							<?php } else { ?>
					<div class="item" style="background-image: url('<?php
					echo esc_url(
						( function_exists( 'pll__' ) ?
							pll__( $slider_content->image_url ) :
							apply_filters( 'wpml_translate_single_string', $slider_content->image_url, 'Repeater', 'Slider Content Image - ' . $slider_content->id ) ) ); ?>');">
						<?php } ?>
						<div class="item-inner">
							<div class="carousel-caption">
								<div class="container">
									<p class="col-md-8 carousel-title">
										<?php
										echo wp_kses_post( force_balance_tags(
											( function_exists( 'pll__' ) ? pll__( html_entity_decode( $slider_content->title ) ) : apply_filters( 'wpml_translate_single_string', html_entity_decode( $slider_content->title ), 'Repeater', 'Slider Content Title - ' . $slider_content->id ) ) ) ); ?>
									</p>
									<div class="col-md-8 carousel-content">
										<?php
										echo wp_kses_post( force_balance_tags(
											( function_exists( 'pll__' ) ? pll__( html_entity_decode( $slider_content->text ) ) : apply_filters( 'wpml_translate_single_string', html_entity_decode( $slider_content->text ), 'Repeater', 'Slider Content Text - ' . $slider_content->id ) ) ) ); ?>
									</div>
									<?php if ( ( ! empty( $slider_content->button_text ) ) && ( ! empty( $slider_content->link ) ) ) : ?>
										<p class="col-md-8 carousel-button">
											<a href=" <?php
											echo esc_url( ( function_exists( 'pll__' ) ? pll__( strip_tags( $slider_content->link ) ) : apply_filters( 'wpml_translate_single_string', strip_tags( $slider_content->link ), 'Repeater', 'Slider Content Link - ' . $slider_content->id ) ) ); ?>"
											   class="slider-button" title="Title">
												<?php
												echo wp_kses_post( force_balance_tags(
														( function_exists( 'pll__' ) ?
															pll__( html_entity_decode( $slider_content->button_text ) ) :
															apply_filters( 'wpml_translate_single_string', html_entity_decode( $slider_content->button_text ), 'Repeater', 'Slider Content Button Text - ' . $slider_content->id )
														) )
												); ?>
											</a>
										</p>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>

					<?php $var1 ++;
}
			}
					?>

				</div>

				<?php if ( $var1 > 1 ) : ?>

					<ol class="carousel-indicators">
						<?php foreach ( $slider_content_decoded as $slider_content ) {
							if ( $slides_counter == 0 ) {
								echo '<li data-target="#main-slider" data-slide-to="' . $slides_counter . '" class="active"></li>';
								$slides_counter ++;
							} else {
								echo '<li data-target="#main-slider" data-slide-to="' . $slides_counter . '"></li>';
								$slides_counter ++;
							}
} ?>
					</ol>
				<?php endif; ?>

				<?php if ( $slides_counter > 1 ) : ?>

					<a class="left carousel-control" href="#main-slider" role="button" data-slide="prev">
						<span class="fa fa-angle-left" aria-hidden="true"></span>
						<span class="sr-only"><?php esc_html_e( 'Previous', 'lawyeriax-lite' ) ?></span>
					</a>
					<a class="right carousel-control" href="#main-slider" role="button" data-slide="next">
						<span class="fa fa-angle-right" aria-hidden="true"></span>
						<span class="sr-only"><?php esc_html_e( 'Next', 'lawyeriax-lite' ) ?></span>
					</a>

				<?php endif; ?>
			</div>
		</section><!-- #slider -->

	<?php endif; ?>

		<?php
	}
endif;

if ( ! function_exists( 'lawyeriax_slider_register_strings' ) ) :

	/**
	 * Register polylang strings
	 */
	function lawyeriax_slider_register_strings() {
		$default = '';
		apply_filters( 'lawyeriax_filter_slider_defaults', $default );
		lawyeriax_pll_string_register_helper( 'lawyeriax_slider_content', $default, 'Slider section' );
	}

endif;

if ( function_exists( 'lawyeriax_slider_register_strings' ) ) {
	add_action( 'after_setup_theme', 'lawyeriax_slider_register_strings', 11 );
}

