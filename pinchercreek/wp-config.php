<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pinchercreek');

/** MySQL database username */
define('DB_USER', 'dbUser1');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~9rW#+F<!=~_TpUYsQbb&DMUSKFsuQSOKZ~WR[J*1:&uIi<b|#x$(g%PGx~GJvl7');
define('SECURE_AUTH_KEY',  '/C+f}$D2k/M~O.l6o[UpE`_Yq 8da(zGF_B<U^uj@zAqDl._f`Qon8@Wt`!JU0`V');
define('LOGGED_IN_KEY',    'wQQW;G=C0?r~/YaPS(Muq~&FSo*^3?h1@b|m;NDYd,.*I3rZluTLu%v-LSz6F%s0');
define('NONCE_KEY',        'Z3u?$cf^ h7.sqWbo{C z|b9mJWybPbD[Nj[[jz5!|MYg2D>f[TcymD2Oa35*n,z');
define('AUTH_SALT',        '9D}b7T.V<@#zcM56PfS.g_}+5<8kuyf!wnk2#]BCd~2QSETS[_m;+GH^|J1o9~|z');
define('SECURE_AUTH_SALT', '`.PYgQ0F{l<Q8^$kI[VmT#wF=5M0$9/T1g|vRmM3093uecV>F5GJa6hdXT1mH(%M');
define('LOGGED_IN_SALT',   'q.;wZOB{5IpXUwshax^K#`JLJgiz*s}nKn^Pn=7hpSIi`t5/|MKIz:2I)Pq,I|vg');
define('NONCE_SALT',       '.^`Yv(!,[1@YR_*aBMtuoBy{R+Lv?HnNohQ6I9iB=dC7wJcjaCY F,ee#!#17e)^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
